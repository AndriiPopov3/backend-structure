const { Router } = require('express');
const jwt = require("jsonwebtoken");
const router = Router();
const statEmitter = require('../emit/stats-emitter');
const { userGetValidation, userPostValidation, userPutValidation } = require('../middlewares/user-validation-middlewares');
const { userAuthValidation } = require('../middlewares/authorization-validation-middlewares');
const { userService } = require('../services/services');
const { toCamelCase } = require('../helpers/caseHelpers');

router.get("/:id", userGetValidation, async (req, res) => {
    try {
        const result = await userService.getOne(req.params.id);
        if(!result) {
          res.status(404).send({ error: 'User not found'});
          return;
        }
        return res.send({ 
          ...result,
        });
    } catch (err) {
        console.log(err);
        res.status(500).send("Internal Server Error");
        return;
    }
});

router.post("/", userPostValidation, async (req, res) => {
    req.body.balance = 0;
    try {
      let result = await userService.add(req.body);
      result = toCamelCase(result, ['created_at', 'updated_at']);
      statEmitter.emit('newUser');
      return res.send({ 
        ...result,
        accessToken: jwt.sign({ id: result.id, type: result.type }, process.env.JWT_SECRET)
      });
    } catch(err) {
      if(err.code == '23505') {
        res.status(400).send({
          error: err.detail
        });
        return;
      }
      res.status(500).send("Internal Server Error");
      return;
    };
  });

  router.put("/:id", userAuthValidation, userPutValidation, async (req, res) => {
    try {
      const result = await userService.update(req.params.id, req.body);
      return res.send({ 
        ...result,
      });
    } catch(err) {
      if(err.code == '23505') {
        console.log(err);
        res.status(400).send({
          error: err.detail
        });
        return;
      }
      res.status(500).send("Internal Server Error");
      return;
    };
  });

module.exports = router;