const userRoutes = require('./users-routes');
const healthRoutes = require('./health-routes');
const statsObject = require("./stats-routes");
const betsRoutes = require("./bets-routes");
const eventsRoutes = require("./events-routes");
const transactionsRoutes = require("./transactions-routes");

module.exports = (app) => {
    app.use('/users', userRoutes);
    app.use('/health', healthRoutes);
    app.use('/stats', statsObject.router);
    app.use('/bets', betsRoutes);
    app.use('/events', eventsRoutes);
    app.use('/transactions', transactionsRoutes);
  };