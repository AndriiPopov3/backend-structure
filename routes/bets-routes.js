const { Router } = require('express');
const router = Router();
const statEmitter = require('../emit/stats-emitter');
const { betPostValidation, authWithId } = require('../middlewares/bet-validation-middlewares');
const { userService, eventService, oddsService, betService } = require('../services/services');
const { to_case, toCamelCase } = require('../helpers/caseHelpers');
const { betResult } = require('../helpers/betHelpers');

router.post("/", betPostValidation, authWithId, async (req, res) => {
    try {
      req.body = to_case(req.body, ['eventId', 'betAmount']);
      req.body.user_id = res.data;
      const user = await userService.getOne(res.data);
      if(!user) {
        res.status(400).send({ error: 'User does not exist'});
        return;
      }
      if(+user.balance < +req.body.bet_amount) {
        return res.status(400).send({ error: 'Not enough balance' });
      }
      const event = await eventService.getOne(req.body.event_id);
      if(!event) {
        return res.status(404).send({ error: 'Event not found' });
      }
      const odds = await oddsService.getOne(event.odds_id);
      if(!odds) {
        return res.status(404).send({ error: 'Odds not found' });
      }
      let multiplier = betResult(req.body.prediction, odds);
      let bet = await betService.add({
        ...req.body,
        multiplier,
        event_id: event.id
      });
      const currentBalance = user.balance - req.body.bet_amount;
      const userUpdated = await userService.update(res.data, { balance: currentBalance, });
      statEmitter.emit('newBet');
      bet = toCamelCase(bet, ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at', 'user_id']);
      return res.send({ 
        ...bet,
        currentBalance: currentBalance,
      });
    } catch (err) {
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    }
  });
  

module.exports = router;