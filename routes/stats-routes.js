const { Router } = require('express');
const db = require('../db/config');
const { AdminAuthValidation } = require('../middlewares/authorization-validation-middlewares');
const { userService, eventService, betService } = require('../services/services');
const router = Router();

const stats = {
    totalUsers: 0,
    totalBets: 0,
    totalEvents: 0,
  };

  userService.count().then(total => {
    stats.totalUsers = total[0].count;
  })
  betService.count().then(total => {
    stats.totalBets = total[0].count;
  })
  eventService.count().then(total => {
    stats.totalEvents = total[0].count;
  })

router.get("/", AdminAuthValidation, (req, res) => {
    try {      
      res.send(stats);
    } catch (err) {
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    }
  });

module.exports = {router, stats};