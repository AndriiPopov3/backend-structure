const { Router } = require('express');
const router = Router();
const { transactionValidation } = require('../middlewares/transaction-validation-middlewares');
const { AdminAuthValidation } = require('../middlewares/authorization-validation-middlewares');
const { userService, transactionService } = require('../services/services');
const { to_case, toCamelCase } = require('../helpers/caseHelpers');

router.post("/", transactionValidation, AdminAuthValidation, async (req, res) => {
    try {
      const user = await userService.getOne(req.body.userId);
        if(!user) {
          res.status(400).send({ error: 'User does not exist'});
          return;
        }
        req.body = to_case(req.body, ['cardNumber', 'userId']);
        let transaction = await transactionService.add(req.body);
        const currentBalance = req.body.amount + user.balance;
        const userUpdated = await userService.update(req.body.user_id, {'balance': currentBalance});
        transaction = toCamelCase(transaction, ['user_id', 'card_number', 'created_at', 'updated_at']);
        return res.send({ 
          ...transaction,
          currentBalance,
        });
    } catch(err) {
      res.status(500).send("Internal Server Error");
      return;
    };
  });

module.exports = router;