const { Router } = require('express');
const router = Router();
const statEmitter = require('../emit/stats-emitter');
const { eventPostValidation, eventPutValidation } = require('../middlewares/event-validation-middlewares');
const { AdminAuthValidation } = require('../middlewares/authorization-validation-middlewares');
const { userService, eventService, oddsService, betService } = require('../services/services');
const { to_case, toCamelCase } = require('../helpers/caseHelpers');
const { eventResult } = require('../helpers/eventHelpers');

router.post("/", AdminAuthValidation, eventPostValidation, async (req, res) => {   
    try {  
      req.body.odds = to_case(req.body.odds, ['homeWin', 'awayWin']);
      let odds = await oddsService.add(req.body.odds);
      delete req.body.odds;
      req.body = to_case(req.body, ['awayTeam', 'homeTeam', 'startAt']);
      const event = await eventService.add({
        ...req.body,
        odds_id: odds.id
      });
      statEmitter.emit('newEvent');
      const newEvent = toCamelCase(event, ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at']);
      odds = toCamelCase(odds, ['home_win', 'away_win', 'created_at', 'updated_at']);
      return res.send({ 
        ...newEvent,
        odds,
      });
    } catch (err) {
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    }
  });
  
  router.put("/:id", AdminAuthValidation, eventPutValidation, async (req, res) => {
    try {
      const eventId = req.params.id;
      const bets = await betService.getAll(eventId);
      const result = eventResult(req.body.score);
      const event = await eventService.update(eventId, { score: req.body.score });
      Promise.all(bets.map(async (bet) => {
        if (bet.prediction === result) {
          const updatedBet = await betService.update(bet.id, { win: true });
          const user = await userService.getOne(bet.user_id);
            return userService.update(bet.user_id, {
              balance: user.balance + (bet.bet_amount * bet.multiplier),
            });
        } else if (bet.prediction !== result) {
          return betService.update(bet.id, { win: false });
        }
      }));
      setTimeout(() => {
        const newEvent = toCamelCase(event, ['bet_amount', 'event_id', 'away_team', 'home_team', 'odds_id', 'start_at', 'updated_at', 'created_at']);
        res.send(newEvent);
      }, 1000)
    } catch (err) {
      console.log(err);
      res.status(500).send("Internal Server Error");
      return;
    }
  });

module.exports = router;