const joi = require('joi');
const jwt = require("jsonwebtoken");

const betPostValidation = (req, res, next) => {
    const schema = joi.object({
        id: joi.string().uuid(),
        eventId: joi.string().uuid().required(),
        betAmount: joi.number().min(1).required(),
        prediction: joi.string().valid('w1', 'w2', 'x').required(),
      }).required();
      const isValidResult = schema.validate(req.body);
      if(isValidResult.error) {
        res.status(400).send({ error: isValidResult.error.details[0].message });
        return;
      } else {
          next();
      }
};

const authWithId = (req, res, next) => {
  let userId;
  let token = req.headers['authorization'];
  if(!token) {
    return res.status(401).send({ error: 'Not Authorized' });
  }
  token = token.replace('Bearer ', '');
  try {
    const tokenPayload = jwt.verify(token, process.env.JWT_SECRET);
    userId = tokenPayload.id;
    res.data = userId;
    next();
  } catch (err) {
    console.log(err);
    return res.status(401).send({ error: 'Not Authorized' });
  }
}

module.exports = {
    betPostValidation,
    authWithId
};