const db = require('../db/config');
const Bet = require('./betService');
const Event = require('./eventService');
const Odds = require('./oddService');
const Transaction = require('./transactionService');
const User = require('./userService');

const betService = new Bet(db);
const eventService = new Event(db);
const oddsService = new Odds(db);
const transactionService = new Transaction(db);
const userService = new User(db);

module.exports = {
    betService,
    eventService,
    oddsService,
    transactionService,
    userService
}