class User {
    constructor(db) {
        this.db = db;
        this.table = "user";
    }

    getOne(id) {
        return this.db(this.table).where('id', id).returning("*").then(([result]) => {
            return result
        })
    }

    add(body) {
        return this.db(this.table).insert(body).returning("*").then(([result]) => {
            return result;
        })
    }

    update(id, body) {
        return this.db(this.table).where('id', id).update(body).returning("*").then(([result]) => { 
            return result;
        })
    }

    count() {
        return this.db(this.table).count("id").then((total) => {
            return total;
          });
    }
}

module.exports = User;