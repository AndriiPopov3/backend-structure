class Odds {
    constructor(db) {
        this.db = db;
        this.table = "odds";
    }

    getOne(id) {
        return this.db(this.table).where('id', id).then(([result]) => {
            return result
        })
    }

    add(body) {
        return this.db(this.table).insert(body).returning("*").then(([result]) => {
            return result;
        })
    }
}

module.exports = Odds;