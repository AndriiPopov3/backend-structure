class Transaction {
    constructor(db) {
        this.db = db;
        this.table = "transaction";
    }

    add(body) {
        return this.db(this.table).insert(body).returning("*").then(([result]) => {
            return result;
        })
    }
}

module.exports = Transaction;