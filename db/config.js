const knex = require("knex");
const dbConfig = require("../knexfile");
const db = knex(dbConfig.development);
db.raw('select 1+1 as result').then(function () {
  }).catch((err) => {
    console.log(err);
    throw new Error('Unable to connect to database');
  });

module.exports = db;