const eventResult = (score) => {
    const [w1, w2] = score.split(":");
    let result;
    if(+w1 > +w2) {
      result = 'w1';
    } else if(+w2 > +w1) {
      result = 'w2';
    } else {
      result = 'x';
    }
    return result;
}

module.exports = {
    eventResult
};