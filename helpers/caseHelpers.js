const toCamelCase = (object, array) => {
    array.forEach(whatakey => {
    const index = whatakey.indexOf('_');
    let newKey = whatakey.replace('_', '');
    newKey = newKey.split('')
    newKey[index] = newKey[index].toUpperCase();
    newKey = newKey.join('');
    object[newKey] = object[whatakey];
    delete object[whatakey];
    });
    console.log(object);
    return object;
}

const to_case = (object, array) => {
    array.forEach(key => {
        let newKey = key.split('');
        let index = 0;
        newKey.forEach(letter => letter.toUpperCase() === letter ? index = newKey.indexOf(letter) : null);
        let new_key = [key.slice(0, index), "_", key.slice(index)].join('');
        new_key = new_key.split('')
        new_key[index+1] = new_key[index+1].toLowerCase();
        new_key = new_key.join('');
        object[new_key] = object[key];
        delete object[key];
    });
    return object;
}

module.exports = {
    toCamelCase,
    to_case
};