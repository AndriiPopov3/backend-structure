const winston = require('winston');
const expressWinston = require('express-winston');
const express = require("express");
const app = express();

const port = 3000;

const statEmitter = require("./emit/stats-emitter");

app.use(express.json());

app.use(expressWinston.logger({
  transports: [
    new winston.transports.Console()
  ],
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.json(),
    winston.format.timestamp()
  ),
  meta: false,
  msg: "HTTP ",
  expressFormat: true,
  colorize: false,
  ignoreRoute: function (req, res) { return false; }
}));

const routes = require('./routes/index');
routes(app);

if(!process.env.DATABASE_PORT ||
   !process.env.DATABASE_HOST ||
   !process.env.DATABASE_NAME ||
   !process.env.DATABASE_USER ||
   !process.env.DATABASE_ACCESS_KEY ||
   !process.env.JWT_SECRET) {
    console.log("Error: environment variable missing");
    process.kill(process.pid, 'SIGTERM');
}

const statsObject = require("./routes/stats-routes");

const server = app.listen(port, () => {
  statEmitter.on('newUser', () => {
    statsObject.stats.totalUsers++;
  });
  statEmitter.on('newBet', () => {
    statsObject.stats.totalBets++;
  });
  statEmitter.on('newEvent', () => {
    statsObject.stats.totalEvents++;
  });

  console.log(`App listening at http://localhost:${port}`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    console.log('Server closed.');
  });
});

// Do not change this line
module.exports = { app };